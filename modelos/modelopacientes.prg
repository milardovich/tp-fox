SET DELETED ON
Define Class ModeloPacientes As Session

   DataSession = 1

   Function GetAllPacientes() as Number 
      Select * from paciente;
       into cursor PacienteCursor nofilter 
      Return _Tally

   Function GetPacienteByNro( nroPaciente as Number ) as Number 
      Select * from paciente ;
       where id = nroPaciente ;
       into cursor PacienteCursor nofilter
      Return _Tally

    FUNCTION AddPaciente(txtnombre as String, txtapellido as String, txtfechanac as String, txtnroafiliado as Integer, idobrasocial as Integer)as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\paciente.dbf" AGAIN
        INSERT INTO "data\tablas\paciente.dbf" (nombre,apellido,fecha_nacimiento,nro_afiliado,id_obra_social) VALUES (txtnombre,txtapellido,txtfechanac,txtnroafiliado,idobrasocial)
        Return _Tally
        
    FUNCTION UpdatePaciente(paciente_id as Integer, txtnombre as String, txtapellido as String, txtfechanac as String, txtnroafiliado as Integer, idobrasocial as Integer) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\paciente.dbf" AGAIN
        UPDATE "data\tablas\paciente.dbf" SET nombre = txtnombre, apellido= txtapellido,fecha_nacimiento = txtfechanac, nro_afiliado=txtnroafiliado, id_obra_social=idobrasocial WHERE id = paciente_id


    FUNCTION RemovePaciente(paciente_id as Integer)
    	SET EXCLUSIVE OFF         
        DELETE FROM paciente WHERE id = paciente_id 
        CLOSE TABLES
        PACK paciente
Enddefine