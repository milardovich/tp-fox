SET DELETED ON
Define Class ModeloProfesionales As Session

   DataSession = 1

   Function GetAllProfesionales() as Number 
      Select * from profesional ;
       into cursor ProfesionalesCursor nofilter 
      Return _Tally

   Function GetProfesionalByNro( nroProfesional as Number ) as Number 
      Select * from profesional ;
       where id = nroProfesional ;
       into cursor ProfesionalesCursor nofilter
      Return _Tally

    FUNCTION AddProfesional(txtnombre as String, txtapellido as String, txtmatricula as String) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\profesional.dbf" AGAIN
        INSERT INTO profesional (apellido,nombre,nro_matricula) VALUES (txtapellido,txtnombre,txtmatricula)
     	USE IN profesional
        Return _Tally
        
    FUNCTION UpdateProfesional(profesional_id as Integer, txtnombre as String, txtapellido as String, txtmatricula as String) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\profesional.dbf" AGAIN IN 0
        UPDATE "data\tablas\profesional.dbf" SET apellido = txtapellido, nombre = txtnombre, nro_matricula = txtmatricula WHERE id = profesional_id
		USE IN profesional

    FUNCTION RemoveProfesional(profesional_id as Integer)
    	SET EXCLUSIVE OFF         
        DELETE FROM profesional WHERE id = profesional_id
        CLOSE TABLES
        PACK profesional
Enddefine