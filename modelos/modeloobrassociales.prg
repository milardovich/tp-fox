SET DELETED ON
Define Class ModeloObrasSociales As Session

   DataSession = 1

   Function GetAllObrasSociales() as Number 
      Select * from obrasocial ;
       into cursor ObrasSocialesCursor nofilter 
      Return _Tally

   Function GetObraSocialByNro( nroObrasocial as Number ) as Number 
      Select * from obrasocial ;
       where id = nroObrasocial ;
       into cursor ObrasSocialesCursor nofilter
      Return _Tally

    FUNCTION AddObraSocial(txtnombre as String, txtcantmax as Integer) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\obrasocial.dbf" AGAIN
        INSERT INTO "data\tablas\obrasocial.dbf" (nombre,cant_max_recetas_x_afiliado) VALUES (txtnombre,txtcantmax)
        Return _Tally
        
    FUNCTION UpdateObraSocial(obrasocial_id as Integer, txtnombre as String, txtcantmax as Integer) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\obrasocial.dbf" AGAIN
        UPDATE "data\tablas\obrasocial.dbf" SET nombre = txtnombre, cant_max_recetas_x_afiliado= txtcantmax WHERE id_obra_social = obrasocial_id


    FUNCTION RemoveObraSocial(obrasocial_id as Integer)
    	SET EXCLUSIVE OFF         
        DELETE FROM obrasocial WHERE id_obra_social = obrasocial_id 
        CLOSE TABLES
        PACK obrasocial
Enddefine