SET DELETED ON
Define Class ModeloPresentaciones As Session

   DataSession = 1

   Function GetAllPresentaciones() as Number 
      Select * from presentacion ;
       into cursor PresentacionesCursor nofilter 
      Return _Tally

   Function GetPresentacionByNro( nroPresentacion as Number ) as Number 
      Select * from presentacion ;
       where id = nroPresentacion ;
       into cursor PresentacionesCursor nofilter
      Return _Tally

    FUNCTION AddPresentacion(txtnombre as String) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\presentacion.dbf" AGAIN
        INSERT INTO "data\tablas\presentacion.dbf"  (nombre) VALUES (txtnombre)
        Return _Tally
        
    FUNCTION UpdatePresentacion(presentacion_id as Integer, txtnombre as String) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\presentacion.dbf" AGAIN
        UPDATE "data\tablas\presentacion.dbf" SET nombre = txtnombre WHERE id = presentacion_id


    FUNCTION RemovePresentacion(presentacion_id as Integer)
    	SET EXCLUSIVE OFF         
        DELETE FROM presentacion WHERE id = presentacion_id 
        CLOSE TABLES
        PACK presentacion
Enddefine