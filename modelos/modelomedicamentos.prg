SET DELETED ON
Define Class ModeloMedicamentos As Session

   DataSession = 1

   Function GetAllObrasSociales() as Number 
      Select * from medicamento ;
       into cursor MedicamentoCursor nofilter 
      Return _Tally

   Function GetMedicamentoByNro( nroMedicamento as Number ) as Number 
      Select * from item ;
       where id = nroMedicamento ;
       into cursor ObrasSocialesCursor nofilter
      Return _Tally

    FUNCTION AddMedicamento(txtefectossec as String, txtnombre as String) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\medicamento.dbf" AGAIN
        INSERT INTO "data\tablas\medicamento.dbf" (desc_efectos_secundarios,nombre) VALUES (txtefectossec,txtnombre)
        
    FUNCTION UpdateMedicamento(medicamento_id as Integer, txtnombre as String, txtefectossec as String) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\medicamento.dbf" AGAIN
        UPDATE "data\tablas\medicamento.dbf" SET nombre = txtnombre, desc_efectos_secundarios= txtefectossec WHERE id = medicamento_id


    FUNCTION RemoveMedicamento(medicamento_id as Integer)
    	SET EXCLUSIVE OFF         
        DELETE FROM medicamento WHERE id = medicamento_id 
        CLOSE TABLES
        PACK medicamento
Enddefine