SET DELETED ON
Define Class ModeloItems As Session

   DataSession = 1

   Function GetAllItems() as Number 
      Select * from item ;
       into cursor ItemsCursor nofilter 
      Return _Tally

   Function GetItemByNro( nroItem as Number ) as Number 
      Select * from item ;
       where id = nroItem ;
       into cursor ItemsCursor nofilter
      Return _Tally

    FUNCTION AddItems(txtstock as Integer, txtprecio as Float, idpres as Integer, idmed as Integer) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\item.dbf" AGAIN
        INSERT INTO "data\tablas\item.dbf" (cant_en_stock,precio,id_presentacion,id_medicamento) VALUES (txtstock,txtprecio,idpres,idmed)
        Return _Tally
        
    FUNCTION UpdateItem(item_id as Integer, txtstock as Integer, txtprecio as Float, idpres as Integer, idmed as Integer) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\item.dbf" AGAIN
        UPDATE "data\tablas\item.dbf" SET cant_en_stock = txtstock, precio= txtprecio , id_presentacion = idpres , id_medicamento = idmed WHERE id = item_id


    FUNCTION RemoveItems(item_id as Integer)
    	SET EXCLUSIVE OFF         
        DELETE FROM item  WHERE id = item_id 
        CLOSE TABLES
        PACK item
Enddefine