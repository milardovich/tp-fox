SET DELETED ON
Define Class ModeloRecetas As Session

   DataSession = 1

   Function GetAllRecetas() as Number 
      Select * from receta ;
       into cursor RecetasCursor nofilter 
      Return _Tally

   Function GetRecetaByNro( nroReceta as Number ) as Number 
      Select * from receta ;
       where id = nroReceta ;
       into cursor RecetasCursor nofilter
      Return _Tally

    FUNCTION AddReceta(txtfecha as String, idpac as Integer , idprof as Integer) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\receta.dbf" AGAIN
        INSERT INTO "data\tablas\receta.dbf" (fecha_venta,id_paciente,id_profesional) VALUES (txtfecha,idpac,idprof)
        Return _Tally
        
    FUNCTION UpdateReceta(receta_id as Integer, txtfecha as String, idpac as Integer, idprof as Integer) as VOID
        SET EXCLUSIVE OFF
        USE "data\tablas\receta.dbf" AGAIN
        UPDATE "data\tablas\receta.dbf" SET fecha_venta = txtfecha, id_paciente= idpac, id_profesional=idprof WHERE id = receta_id


    FUNCTION RemoveReceta(receta_id as Integer)
    	SET EXCLUSIVE OFF         
        DELETE FROM receta WHERE id = receta_id 
        CLOSE TABLES
        PACK receta
Enddefine