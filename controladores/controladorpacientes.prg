Define Class ControladorPacientes As Session

   Protected Model, View
   Model = null
   View = null


   Function Init() as Boolean 

      this.Model = NewObject( "ModeloPacientes", "modelos\ModeloPacientes.prg" )

      With this.Model as ModeloPacientes of ModeloPacientes.prg
         .GetAllProfesionales()
         .GetProfesionalByNro()
      EndWith 

   Function ObrasPacientesList() as VOID

      Do form VistaProfesionales name this.View
      this.View.DataSessionID = this.DataSessionId
      this.View.Initialize()
      this.SetEventHandlers()
      this.RefreshMoreData(0)

    FUNCTION AddPaciente(txtnombre as String, txtapellido as String, txtfechanac as Date, txtnroafiliado as Integer, idnroafiliado as Integer, idobrasocial as Integer) as VOID
        this.Model.AddPaciente(txtnombre,txtapellido,txtfechanac,txtnroafiliado,idnroafiliado,idobrasocial)

    FUNCTION RemovePaciente(paciente_id as Integer)
        this.model.RemovePaciente(paciente_id)   
    
    FUNCTION GetPacienteByNro (paciente_id as Integer) as Number
    	RETURN this.model.GetObraSocialByNro(paciente_id)




EndDefine